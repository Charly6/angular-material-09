import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CardI } from '../interfaces/tree.interfaces';

@Injectable({
  providedIn: 'root'
})
export class NodeService {

  constructor(private http: HttpClient) { }

  getMenu(): Observable<CardI[]> {
    return this.http.get<CardI[]>('./assets/data/node.json');
  }
}
