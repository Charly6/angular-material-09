import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormArray, FormBuilder, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import { NodeService } from 'src/app/services/node.service';

@Component({
  selector: 'app-my-form',
  templateUrl: './my-form.component.html',
  styleUrls: ['./my-form.component.css']
})
export class MyFormComponent implements OnInit {
  formaUtils!: FormGroup;
  fb: FormBuilder
  mensaje1 = ""
  guardados: {name:any}[] = []

  constructor(fb: FormBuilder,private _nodeService: NodeService) {
    this.fb = fb
    this.formaUtils = this.fb.group({
      niveles: [1, [Validators.min(1), Validators.max(100), Validators.required]],
      materials: this.fb.array([
        this.addMaterialFormGroup()
      ])
    });
  }

  get materialsList() {
    return this.formaUtils.controls["materials"] as FormArray;
  }

  conditionOfMaterial(i:number) {
    return !this.materialsList.controls[i].value.condition && this.materialsList.controls[i].touched
  }

  addMaterialFormGroup(): FormGroup{
    return this.fb.group({
      name:["", [Validators.required, Validators.pattern('[A-Za-z0-9 ]+$')]],
      condition: [false, [Validators.required, this.aceptarObligatorio]]
    })
  }

  aceptarObligatorio(control: AbstractControl): ValidationErrors| null {
    if (control.value === true) {
      return null
    }
      return { aceptarObligatorio: control.value }
}

  addMaterialButtonClick(): void {
    console.log(this.materialsList.controls[0]);
    (<FormArray>this.formaUtils.get('materials')).push(this.addMaterialFormGroup())
  }

  ngOnInit(): void {}

  eliminar(lessonIndex: number) {
    this.materialsList.removeAt(lessonIndex);
  }

  saveAll() {
    let recuperado = this.materialsList.controls.filter((data) => data.valid === true)
    recuperado.forEach((data => {
      let dataNew = {
        name: data.value.name.trim()
      }
      this.guardados.push(dataNew)
    }))
    console.log(this.guardados);
  }

  deletAll() {
    this.materialsList.clear()
  }

  limpiar() {
    this.materialsList.controls.forEach(data => {
      data.reset()
    })
  }
}
